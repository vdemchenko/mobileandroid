package com.livehealthier.mobile.api;

import android.app.Activity;
import android.os.AsyncTask;

import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LH_AuthHandler extends AsyncTask<String, Integer, Integer> {

	Activity _context;

	public LH_AuthHandler(Activity context) {
		_context = context;
	}

	public void logout(Activity context) {
		try {

			String token = LH_Utils.getWebToken(context);

			LH_LogHelper.logInfo("User Token On Logout " + token);

		
			
			
			String getResponse = LHHttpConnection.getResponseFromGetRequest(
					LHApplication.CurrentEnvironment.getApiBaseUrl()
							+ LH_ApiUrls.MOBILEAPPVERSIONID, token);

			LH_LogHelper.logInfo("logout response - " + getResponse);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected Integer doInBackground(String... params) {
		logout(_context);
		return 1;
	}

}
