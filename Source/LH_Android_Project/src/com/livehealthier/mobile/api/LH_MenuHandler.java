package com.livehealthier.mobile.api;

import android.app.Activity;

import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_MenuItems;
import com.livehealthier.mobile.model.LH_SubMenu;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;

public class LH_MenuHandler {

	public void getMenu(Activity context) {

		String token = LH_Utils.getWebToken(context);
		String clientID = LH_Utils.getclientid(context);
		String officeID = LH_Utils.getOfficeId(context);

		LH_LogHelper.logInfo("Menu Token - " + token);

		try {
			String getResponse = LHHttpConnection.getResponseFromGetRequest(
					LHApplication.CurrentEnvironment.getApiBaseUrl()
							+ LH_ApiUrls.MENUAPI + clientID + "?OfficeID="
							+ officeID, token);

			LH_LogHelper.logInfo("request for menu - "
					+ LHApplication.CurrentEnvironment.getApiBaseUrl()
					+ LH_ApiUrls.MENUAPI + clientID + "?OfficeID=" + officeID);
			
			LH_LogHelper.logInfo("response for menu - " + getResponse);

			try {
				LH_Utils.deleteMenutable(context);
			} catch (Exception e1) {
				e1.printStackTrace();
			}

			LHApplication.Menu = LH_Utils.menuparser(getResponse);

			LHApplication.ClearCookies();

		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			int header = 0;
			for (LH_MenuItems MenuItems : LHApplication.Menu
					.getLhmenuitemList()) {

				if (!MenuItems.getHasSubMenu().contains("true")) {
					LH_Utils.saveMenuInformation(context, LHApplication.Menu,
							header, 0);

					try {
						LHApplication.ComponentRegistry.setTitle(
								MenuItems.getUrl(), MenuItems.getTitle());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {

					int child = 0;
					for (LH_SubMenu submenu : LHApplication.Menu
							.getLhmenuitemList().get(header).getSubMenu()) {
						LH_Utils.saveMenuInformation(context,
								LHApplication.Menu, header, child);

						try {
							LHApplication.ComponentRegistry.setTitle(
									submenu.getUrl(), submenu.getTitle());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						child++;
					}

				}
				header++;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
