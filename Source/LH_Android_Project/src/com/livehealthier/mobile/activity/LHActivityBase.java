package com.livehealthier.mobile.activity;

import java.util.HashMap;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.ExpandableListView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.livehealthier.mobile.R;
import com.livehealthier.mobile.adapter.ExpandableListAdapter;
import com.livehealthier.mobile.fragment.LH_Menu_List_Fragment;

public class LHActivityBase extends SlidingFragmentActivity {

	ExpandableListAdapter listAdapter;
	ExpandableListView expListView;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;

	private int mTitleRes;
	protected Fragment mFrag;
	private static long back_pressed;

	@SuppressWarnings("static-access")
	public LHActivityBase(int titleRes) {
		mTitleRes = titleRes;
		// new LHExceptionHandler().uncaughtException(Thread.currentThread(),
		// new IllegalArgumentException(
		// try {
		// Thread.currentThread().setDefaultUncaughtExceptionHandler(new
		// LH_ExceptionHandler(getApplicationContext()));
		//
		// throw new Exception("LH application  exception ");
		//
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setTitle(mTitleRes);

		// set the Behind View
		setBehindContentView(R.layout.menu_frame);

		if (savedInstanceState == null) {
			FragmentTransaction t = this.getSupportFragmentManager()
					.beginTransaction();
			mFrag = new LH_Menu_List_Fragment();
			t.replace(R.id.menu_frame, mFrag);
			t.commit();
		} else {
			mFrag = this.getSupportFragmentManager().findFragmentById(
					R.id.menu_frame);
		}

		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

		// sm.showContent();

		getSupportActionBar().setIcon(R.drawable.ic_drawer);
		getSupportActionBar().setHomeButtonEnabled(true);
		// getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_drawer));
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);

	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu);
		menu.clear();
		menu.clear();
		inflater.inflate(R.menu.action_menu, menu);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			toggle();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
