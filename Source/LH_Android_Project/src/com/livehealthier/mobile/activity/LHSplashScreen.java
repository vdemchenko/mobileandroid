package com.livehealthier.mobile.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.google.gson.Gson;
import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LoadingTask.LoadingTaskFinishedListener;
import com.livehealthier.mobile.api.LHComponentRegistryAPI;
import com.livehealthier.mobile.api.LH_AppVersionAPI;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistry;
import com.livehealthier.mobile.component_registry.LH_ComponentRegistryMap;
import com.livehealthier.mobile.environment.LH_EnvironmentHandler;
import com.livehealthier.mobile.http.LHConnectionDetector;
import com.livehealthier.mobile.model.LH_Menu;
import com.livehealthier.mobile.model.LH_MenuItems;
import com.livehealthier.mobile.model.LH_SubMenu;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_ExceptionHandler;
import com.livehealthier.mobile.utils.LH_LogHelper;
import com.livehealthier.mobile.utils.LH_Utils;
import com.livehealthier.mobile.utils.utils;
import com.livehealthier.mobile.utils.LH_Constants.Environments;

public class LHSplashScreen extends Activity implements
		LoadingTaskFinishedListener {
	private static final String SHARED_PREFS_FILE = "ComponentRegistry";
	public static LHSplashScreen lhSplashContext;
	LHConnectionDetector connectionDetector;
	public static LH_ComponentRegistry ComponentRegistry;
	TextView SplashText;
	TextView SplashTextLogo;
	TextView Splashcopyrights;
	String deepLinkUrl;
	String deepLinkqueryUrl;
	Boolean itemFound = false;
	int index = 0;
	String Title = "";
	String Url = "";
	Boolean DeeplinkUrlfound = false;

	private void completeSplash() {

		if (connectionDetector.isConnectingToInternet()) {

			startApp();
			finish();
		}// Don't forget to finish this Splash Activity so the user
			// can't return to it!
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Show the splash screen
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();

		setContentView(R.layout.lh_spalsh);

		SplashText = (TextView) findViewById(R.id.textView2);
		SplashTextLogo = (TextView) findViewById(R.id.textView1);
		Splashcopyrights = (TextView) findViewById(R.id.copyright_textView);
		Typeface custom_font_bold = Typeface.createFromAsset(getAssets(),
				"fonts/Avenir Light Oblique.ttf");
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Avenir.ttf");

		SplashText.setTypeface(custom_font);
		SplashTextLogo.setTypeface(custom_font_bold);

		Splashcopyrights.setTypeface(custom_font_bold);

		LH_LogHelper.logInfo("Screen size -"
				+ utils.getSizeName(getApplicationContext()));

		lhSplashContext = this;

		String screenName = getApplicationContext().getResources().getString(
				R.string.GA_Splash_Screen);
		LHApplication.analyticsHandler.screenAnalyticsHits(
				getApplicationContext(), screenName);

		// Find the progress bar
		ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		// Start your loading

		String logintoken = LH_Utils
				.readlogintoken(LHSplashScreen.this, "read");

		connectionDetector = new LHConnectionDetector(lhSplashContext);

		if (connectionDetector.isConnectingToInternet()) {

			if (logintoken.equalsIgnoreCase("livehealthier")) {

				new LoadingTask(progressBar, this, lhSplashContext, false)
						.execute("Display Splash Screen");

			} else {

				new LoadingTask(progressBar, this, lhSplashContext, true)
						.execute("Download Menu");

			}

		} else {

			final AlertDialog alertDialog = new AlertDialog.Builder(
					lhSplashContext).create();
			alertDialog.setTitle(lhSplashContext.getResources().getString(
					R.string.Network_heading));
			alertDialog.setMessage(lhSplashContext.getResources().getString(
					R.string.Network_msg));
			alertDialog.setCancelable(false);
			alertDialog.setButton(
					lhSplashContext.getResources().getString(
							R.string.Network_setting),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							try {
								startActivity(new Intent(
										Settings.ACTION_SETTINGS));
								finish();
							} catch (Exception e) {
								e.printStackTrace();
							}

						}
					});

			alertDialog.setButton2(
					lhSplashContext.getResources().getString(R.string.Cancel),
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							finish();

						}
					});

			alertDialog.show();

		}

	}

	public static void saveComponentRegistry(
			ArrayList<LH_ComponentRegistryMap> _componentRegistryMap) {
		SharedPreferences prefs = lhSplashContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		Editor editor = prefs.edit();
		try {

			Gson gson = new Gson();
			String jsonFavorites = gson.toJson(_componentRegistryMap);

			editor.putString("ComponentRegistry", jsonFavorites);

			editor.commit();

			// editor.putString("ComponentRegistry",
			// _componentRegistryMap.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		editor.commit();

		LHApplication.ComponentRegistry.initializeRegistry();

	}

	public static ArrayList<LH_ComponentRegistryMap> ReadComponentRegistry() {

		SharedPreferences prefs = lhSplashContext.getSharedPreferences(
				SHARED_PREFS_FILE, Context.MODE_PRIVATE);
		List<LH_ComponentRegistryMap> componentRegistryMap = null;

		try {

			String componentRegistry = prefs.getString("ComponentRegistry", "");

			Gson gson = new Gson();

			LH_ComponentRegistryMap[] componentRegistryItems = gson.fromJson(
					componentRegistry, LH_ComponentRegistryMap[].class);

			componentRegistryMap = Arrays.asList(componentRegistryItems);
			componentRegistryMap = new ArrayList<LH_ComponentRegistryMap>(
					componentRegistryMap);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return (ArrayList<LH_ComponentRegistryMap>) componentRegistryMap;

	}

	// This is the callback for when your async task has finished
	@Override
	public void onTaskFinished() {
		completeSplash();
	}

	private void startApp() {

		String logintoken = LH_Utils
				.readlogintoken(LHSplashScreen.this, "read");
		String newdeplinkqueryurl = "";
		try {
			Intent intent = getIntent();

			if (null != intent) {
				// Uri uri = intent.getData();
				try {

					if (null != intent.getData()) {
						deepLinkUrl = intent.getData().getPath();
						deepLinkqueryUrl = intent.getData().getQuery();
						String sourcetype = "";

						if (null != deepLinkqueryUrl) {

							if (deepLinkqueryUrl.contains("&")) {
								StringTokenizer st = new StringTokenizer(
										deepLinkqueryUrl, "&");

								while (st.hasMoreTokens()) {

									String Temp = st.nextToken();
									if (Temp.contains("ec=")) {

										Temp = Temp.substring(3);
										sourcetype = Temp;
									} else {
										newdeplinkqueryurl = Temp;

									}

								}
							} else {

								if (deepLinkqueryUrl.contains("ec=")) {

									sourcetype = deepLinkqueryUrl
											.substring(deepLinkqueryUrl
													.indexOf("ec=") + 3);

									newdeplinkqueryurl = "";
								} else {
									newdeplinkqueryurl = deepLinkqueryUrl;
								}

							}

							LH_LogHelper.logInfo("deepLink source-"
									+ sourcetype);

							// LHApplication.analyticsHandler.screenAnalyticsHits(
							// getApplicationContext(), "Deeplinks -"
							// + sourcetype);

							int mobile_campaign_index = 6;
							if (LH_EnvironmentHandler.getInstance().equals(
									Environments.QA)) {

								mobile_campaign_index = 6;

							} else {

								mobile_campaign_index = 7;
							}

							LHApplication.analyticsHandler
									.eventAnalyticsHits(
											getApplicationContext(),
											"" + sourcetype,
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Category),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Action_DeepLink_item_pressed),
											"" + sourcetype,
											sourcetype, mobile_campaign_index);

						}// ?challengeId=108&abc=12&ec=sms
							// ?ec=EMAIL

					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (!logintoken.equalsIgnoreCase("livehealthier")) {

				LHApplication.ComponentRegistry.initializeRegistry();

			}

			if (null != deepLinkqueryUrl) {

				deepLinkUrl = deepLinkUrl.substring(1);
				if (deepLinkUrl.contains("v2/index.aspx")) {
					if (newdeplinkqueryurl.contains("challengeId")) {

						deepLinkUrl = LHApplication.CurrentEnvironment
								.getgadgetDeeplinkBaseUrl();
					}

				}

				if (!newdeplinkqueryurl.isEmpty()) {
					deepLinkUrl = deepLinkUrl + "?" + newdeplinkqueryurl;
				}

				// deepLinkUrl = deepLinkUrl.substring(0,
				// deepLinkUrl.indexOf("ec=")-1);
			}
			LH_LogHelper.logInfo(" uri -" + intent.getDataString());
			LH_LogHelper.logInfo("deepLinkUrl-" + deepLinkUrl);

			if (LHApplication.ComponentRegistry
					.getdeeplinkComponentItem(deepLinkUrl)) {

				for (LH_MenuItems Menu : LHApplication.Menu.getLhmenuitemList()) {

					// LH_LogHelper.logInfo("Menu url loop -"
					// + Menu.getUrl().toString());

					if (Menu.getUrl().toString().contains(deepLinkUrl)) {

						index = LHApplication.Menu.getLhmenuitemList().indexOf(
								Menu);
						DeeplinkUrlfound = true;
						Title = Menu.getTitle().toString();
						Url = Menu.getUrl().toString();
						break;

					}

				}

				if (!DeeplinkUrlfound) {
					Url = deepLinkUrl;
					Title = "Live Healthier";
				}

			}

		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		if (logintoken.equalsIgnoreCase("livehealthier")) {

			Intent intent = new Intent(LHSplashScreen.this,
					com.livehealthier.mobile.fragment.LH_Login_Home.class);

			if (null != Title && Title.length() > 2) {
				intent.putExtra(LH_Constants._TITLE, Title);
				intent.putExtra(LH_Constants._URL, Url);
			}
			startActivity(intent);
		} else {

			if (null != LHApplication.Menu
					&& LHApplication.Menu.getLhmenuitemList().size() > 0) {

				LHApplication.ComponentRegistry
						.setLastUserInteractionItem(LHApplication.Menu
								.getLhmenuitemList().get(index).getUrl());
			}

			Intent intent = new Intent(LHSplashScreen.this,
					com.livehealthier.mobile.activity.LH_Activity.class);

			if (null != Title && Title.length() > 2) {
				intent.putExtra(LH_Constants._TITLE, Title);
				intent.putExtra(LH_Constants._URL, Url);
			}
			startActivity(intent);

			LHApplication.analyticsHandler.screenAnalyticsHits(
					getApplicationContext(), "Auto Login");

		}
	}
}