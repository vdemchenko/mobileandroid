/**
 * 
 */
package com.livehealthier.mobile.utils;

import java.lang.Thread.UncaughtExceptionHandler;

import android.content.Context;
import android.content.Intent;

import com.livehealthier.mobile.activity.LHApplication;

/**
 * @author bhavesh.kumar
 * 
 */
public class LH_ExceptionHandler implements UncaughtExceptionHandler {

	private final Context myContext;

	public LH_ExceptionHandler(Context applicationContext) {
		myContext = applicationContext;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable throwable) {

		Intent intent = new Intent(myContext,
				com.livehealthier.mobile.fragment.LH_Home.class);
		myContext.startActivity(intent);
		try {
			LHApplication.analyticsHandler.exceptionAnalyticsHits(myContext,
					"Exception", throwable);

			throw new Exception("LH application exception ");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
