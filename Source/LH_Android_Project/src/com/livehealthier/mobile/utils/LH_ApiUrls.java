package com.livehealthier.mobile.utils;

public class LH_ApiUrls {
	// public static final String BASEURL =
	// "http://api.v2.qa-web01.livehealthier.com/";
	// public static final String BASEURL =
	// "http://lhmobileapi.staging.livehealthier.com/";
	// public static final String BASEURL =
	// "http://qa2.api.v2.qa-web01.livehealthier.com/";
	// public static final String BASEURL = "http://api.dev.livehealthier.com/";

	// public static final String BASEURL = "https://lhapi.livehealthier.com/";

	public static final String AUTHENICATEAPI = "Users/Authenticate";

	public static final String MENUAPI = "Menu/";

	public static final String USERAPI = "Users/me";

	public static final String LOGOUTAPI = "Users/logout";
	public static final String COMPONENTREGISTRYAPI = "ComponentRegistry?VersionNumber=";
	public static final String MOBILEAPPVERSIONID = "MobileAppVersion?VersionNumber=";
}
