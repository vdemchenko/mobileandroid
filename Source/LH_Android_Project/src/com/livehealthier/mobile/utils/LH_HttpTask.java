package com.livehealthier.mobile.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonParser;
import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.activity.LH_Activity;
import com.livehealthier.mobile.api.LH_MenuHandler;
import com.livehealthier.mobile.fragment.LH_Login_Direct;
import com.livehealthier.mobile.fragment.LH_Login_Home;
import com.livehealthier.mobile.fragment.LH_Login_SSO;
import com.livehealthier.mobile.http.LHHttpConnection;
import com.livehealthier.mobile.model.LH_Login;

public class LH_HttpTask extends AsyncTask<String, String, String> {

	private final String url;
	private final String requesttype;
	private final List<NameValuePair> pairs;
	private JsonParser jp = null;
	private Activity m_context;
	LH_Login user;
	String deeplinkTitle ="";
	String deeplinkUrl ="";
	// ProgressDialog ringProgressDialog = null;
	List<String> listDataHeader;
	HashMap<String, List<String>> listDataChild;

	public LH_HttpTask(String url, List<NameValuePair> pairs, Activity context,
			String type) {
		this.url = url;
		this.pairs = pairs;
		this.m_context = context;
		this.requesttype = type;
		// dialog = new ProgressDialog(m_context);

	}
	
	public LH_HttpTask(String url, List<NameValuePair> pairs, Activity context,
			String type,String deeplinkTitle,String deeplinkUrl) {
		this.url = url;
		this.pairs = pairs;
		this.m_context = context;
		this.requesttype = type;
		this.deeplinkTitle = deeplinkTitle;
		this.deeplinkUrl = deeplinkUrl;
		// dialog = new ProgressDialog(m_context);

	}


	@SuppressWarnings("deprecation")
	@Override
	protected String doInBackground(String... params) {
		String returnMsg = "";

		try {
			String _postresponse = null;
			try {
				_postresponse = LHHttpConnection._lhPostconnection(url, pairs);
			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			if (requesttype.equalsIgnoreCase("Login")
					|| requesttype.equalsIgnoreCase("SSO")) {

				LHApplication.TokenUpdatedAt = new Date();

				try {
					user = LH_Utils.loginParser(_postresponse, "Login");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (user.getStatus().equalsIgnoreCase("success")) {
					try {
						LH_Utils.deleteUsertable(m_context);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					try {
						LH_Utils.saveLoginInformation(m_context, user);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					returnMsg = user.getException();
					return returnMsg;
				}

			}

			if (user.getUser().get(0).getToken().toString() != "") {

				try {
					LH_Utils.savelogintoken(m_context, user.getUser().get(0)
							.getToken().toString());
					LHApplication.LoginPageRedirectCounter = 0;

				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				try {
					LH_MenuHandler menuHandler = new LH_MenuHandler();
					
					menuHandler.getMenu(m_context);
					
					// getMenuinformation();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				returnMsg = "Success";
				return returnMsg;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return returnMsg;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void onPostExecute(String response) {

		// ringProgressDialog.dismiss();
		LHApplication.getComponentRegistryUpdate("Login");
		if (requesttype.equalsIgnoreCase("Login")) {
			LH_Login_Direct.loadingimg.setVisibility(View.INVISIBLE);
			LH_Login_Direct.logindirect.setPressed(false);
			LH_Login_Direct.logindirect.setClickable(true);
			LH_Login_Direct.logindirect.setFocusable(false);
		} else {

			LH_Login_SSO.loadingimg.setVisibility(View.INVISIBLE);
			LH_Login_SSO.loginpasscode.setPressed(false);
			LH_Login_SSO.loginpasscode.setClickable(true);
			LH_Login_SSO.loginpasscode.setFocusable(false);
		}

		if (response != null && response.equalsIgnoreCase("Success")) {
			try {
				if (null != LH_Login_Direct.lhloginactivity) {
					LH_Login_Direct.lhloginactivity.finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (null != LH_Login_Home.lhloginhomeactivity) {
					LH_Login_Home.lhloginhomeactivity.finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (null != LH_Login_SSO.lhloginssoactivity) {
					LH_Login_SSO.lhloginssoactivity.finish();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				LH_Activity.lhActivity.finish();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (null != LHApplication.Menu
					&& LHApplication.Menu.getLhmenuitemList().size() > 0) {
				LHApplication.ComponentRegistry
						.setLastUserInteractionItem(LHApplication.Menu
								.getLhmenuitemList().get(0).getUrl());
			}
			Intent intent = new Intent(m_context,
					com.livehealthier.mobile.activity.LH_Activity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			
			if(deeplinkTitle.length()>2){
			intent.putExtra(LH_Constants._TITLE, deeplinkTitle);
			intent.putExtra(LH_Constants._URL, deeplinkUrl);
			}
			m_context.startActivity(intent);
		
		
		
		} else if (response != null && response.toString() != "") {

			final AlertDialog alertDialog = new AlertDialog.Builder(m_context)
					.create();
			alertDialog.setTitle(m_context.getResources().getString(
					R.string.app_name));
			alertDialog.setMessage(response);
			alertDialog.setCancelable(false);
			alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

					alertDialog.cancel();

				}
			});

			// alertDialog.setIc

			alertDialog.show();

			// Toast.makeText( m_context, response, Toast.LENGTH_LONG).show();

		} else {

			if (requesttype.equalsIgnoreCase("Login")) {
				Toast.makeText(
						m_context,
						m_context.getResources().getString(
								R.string.invaliduserpassword),
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(
						m_context,
						m_context.getResources().getString(
								R.string.invalidtoken), Toast.LENGTH_LONG)
						.show();
			}
		}
	}

	@Override
	protected void onPreExecute() {
		if (requesttype.equalsIgnoreCase("Login")) {
			LH_Login_Direct.loadingimg.setVisibility(View.VISIBLE);
		} else {

			LH_Login_SSO.loadingimg.setVisibility(View.VISIBLE);
		}
		// ringProgressDialog = ProgressDialog.show(m_context, m_context
		// .getResources().getString(R.string.app_name), "Log in ....",
		// true);
		// ringProgressDialog.setCancelable(false);

		super.onPreExecute();

	}

	@Override
	protected void onProgressUpdate(String... progress) {

	}

}
