package com.livehealthier.mobile.component_registry;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.livehealthier.mobile.model.LH_ComponentRegistryItems;

public class LH_ComponentRegistryMap {
//	private String _keyPartUrl;
	
	private List<String> parturls;
//	private String _destinationPartUrl;
	private String _title;
	private String _destinationFullUrl;
	private String _type;
	private Boolean _isMenu;
	private Boolean _isComponentRegistry;

	private String _componentRegistryId;

	private String _featureName;

	private String _featureType;

	private String _featureSource;

	public LH_ComponentRegistryMap() {

	}

//	public String getKeyPartUrl() {
//		return _keyPartUrl;
//	}

//	public void setKeyPartUrl(String keyPartUrl) {
//		this._keyPartUrl = keyPartUrl;
//	}
//
//	public String getDestinationPartUrl() {
//		return _destinationPartUrl;
//	}
//
//	public void setDestinationPartUrl(String destinationPartUrl) {
//		this._destinationPartUrl = destinationPartUrl;
//	}

	public String getTitle() {
		return _title;
	}

	public void setTitle(String title) {
		this._title = title;
	}

	public String getDestinationFullUrl() {
		return _destinationFullUrl;
	}

	public void setDestinationFullUrl(String destinationFullUrl) {
		this._destinationFullUrl = destinationFullUrl;
	}

	public String getType() {
		return _type;
	}

	public void setType(String type) {
		this._type = type;
	}

	public String get_componentRegistryId() {
		return _componentRegistryId;
	}

	public void set_componentRegistryId(String _componentRegistryId) {
		this._componentRegistryId = _componentRegistryId;
	}

	public String get_featureName() {
		return _featureName;
	}

	public void set_featureName(String _featureName) {
		this._featureName = _featureName;
	}

	public String get_featureType() {
		return _featureType;
	}

	public void set_featureType(String _featureType) {
		this._featureType = _featureType;
	}

	public String get_featureSource() {
		return _featureSource;
	}

	public void set_featureSource(String _featureSource) {
		this._featureSource = _featureSource;
	}

	public List<String> getParturls() {
		return parturls;
	}

	public void setParturls(List<String> parturls) {
		this.parturls = parturls;
	}

	public Boolean get_isMenu() {
		return _isMenu;
	}

	public void set_isMenu(Boolean _isMenu) {
		this._isMenu = _isMenu;
	}

	public Boolean get_isComponentRegistry() {
		return _isComponentRegistry;
	}

	public void set_isComponentRegistry(Boolean _isComponentRegistry) {
		this._isComponentRegistry = _isComponentRegistry;
	}

}
