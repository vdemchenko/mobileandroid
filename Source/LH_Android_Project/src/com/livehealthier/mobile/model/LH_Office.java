package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_Office {
	@JsonProperty("ClientAccountID")
	private String ClientAccountID;
	@JsonProperty("GMTOffset")
	private String GMTOffset;
	@JsonProperty("IsEnabledOnSelfReg")
	private String IsEnabledOnSelfReg;
	@JsonProperty("OfficeCode")
	private String OfficeCode;
	@JsonProperty("OfficeName")
	private String OfficeName;
	@JsonProperty("RegionCode")
	private String RegionCode;
	@JsonProperty("SchedulerCode")
	private String SchedulerCode;
	@JsonProperty("ScreeningExpirationDate")
	private String ScreeningExpirationDate;
	@JsonProperty("ScreeningExpiryDays")
	private String ScreeningExpiryDays;
	@JsonProperty("TimeZoneName")
	private String TimeZoneName;

	@JsonProperty("Client")
	private List<LH_Client> Client;

	public List<LH_Client> getClient() {
		return Client;
	}

	public String getClientAccountID() {
		return ClientAccountID;
	}

	public String getGMTOffset() {
		return GMTOffset;
	}

	public String getIsEnabledOnSelfReg() {
		return IsEnabledOnSelfReg;
	}

	public String getOfficeCode() {
		return OfficeCode;
	}

	public String getOfficeName() {
		return OfficeName;
	}

	public String getRegionCode() {
		return RegionCode;
	}

	public String getSchedulerCode() {
		return SchedulerCode;
	}

	public String getScreeningExpirationDate() {
		return ScreeningExpirationDate;
	}

	public String getScreeningExpiryDays() {
		return ScreeningExpiryDays;
	}

	public String getTimeZoneName() {
		return TimeZoneName;
	}

	public void setClient(List<LH_Client> client) {
		Client = client;
	}

	public void setClientAccountID(String clientAccountID) {
		ClientAccountID = clientAccountID;
	}

	public void setGMTOffset(String gMTOffset) {
		GMTOffset = gMTOffset;
	}

	public void setIsEnabledOnSelfReg(String isEnabledOnSelfReg) {
		IsEnabledOnSelfReg = isEnabledOnSelfReg;
	}

	public void setOfficeCode(String officeCode) {
		OfficeCode = officeCode;
	}

	public void setOfficeName(String officeName) {
		OfficeName = officeName;
	}

	public void setRegionCode(String regionCode) {
		RegionCode = regionCode;
	}

	public void setSchedulerCode(String schedulerCode) {
		SchedulerCode = schedulerCode;
	}

	public void setScreeningExpirationDate(String screeningExpirationDate) {
		ScreeningExpirationDate = screeningExpirationDate;
	}

	public void setScreeningExpiryDays(String screeningExpiryDays) {
		ScreeningExpiryDays = screeningExpiryDays;
	}

	public void setTimeZoneName(String timeZoneName) {
		TimeZoneName = timeZoneName;
	}

	@Override
	public String toString() {
		return "LH_Office [ClientAccountID=" + ClientAccountID + ", GMTOffset="
				+ GMTOffset + ", IsEnabledOnSelfReg=" + IsEnabledOnSelfReg
				+ ", OfficeCode=" + OfficeCode + ", OfficeName=" + OfficeName
				+ ", RegionCode=" + RegionCode + ", SchedulerCode="
				+ SchedulerCode + ", ScreeningExpirationDate="
				+ ScreeningExpirationDate + ", ScreeningExpiryDays="
				+ ScreeningExpiryDays + ", TimeZoneName=" + TimeZoneName
				+ ", Client=" + Client + "]";
	}

}
