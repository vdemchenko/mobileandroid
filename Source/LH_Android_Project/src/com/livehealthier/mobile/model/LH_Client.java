package com.livehealthier.mobile.model;

public class LH_Client {

	private String CAHeaderMovement;
	private String CAHeaderNutrition;
	private String ClientAccountCode;
	private String MobileLogo;
	private String ClientAccountName;
	private String ClientCustomerServiceEmailID;
	private String ClientSystemEmailID;

	private String CommentFromEmail;
	private String CommentToEmail;
	private String CommunityAdminUserID;

	private String CustomerServicePhone;
	private String DefaultExpertName;
	private String DefaultFriendUserID;

	private String DisableOnlineCoaching;
	private String DisableOnsiteCoaching;
	private String DisableSiteHeader;

	private String DisableTelephonicCoaching;

	private String IsScreeningToDoBlockEnabled;

	private String ExternalLogoutUrl;
	private String FullDomainName;
	private String HideOfficeSelection;
	private String HideTopnav;
	private String IsActive;
	private String IsCountryBasedClient;
	private String IsDualLoginEnabled;
	private String IsFullDomainNameSupported;
	private String IsHVSyncDisable;
	private String IsHVToDoBlockEnabled;
	private String IsLeftNavEnabled;
	private String IsMobileEnabled;
	private String IsPostNotificationEnable;
	private String IsRememberMeHidden;

	private String IsSilverlightDesktopInstallationSupported;
	private String IsSLDesktopInstallationDisabled;
	private String IsSSOEnabled;
	private String IsWellnessScoreDisabled;
	private String NGCLobCode;
	private String NurturCode;
	private String PortalWidth;
	private String PrimaryColor;
	private String RawScreeningSeason;
	private String RemoveCASpecLinks;
	private String SchedulerEmailFromAddress;
	private String ScreeningReminderEmailsEnabled;
	private String ScreeningSeason;
	private String Season;
	private String SecondaryColor;
	private String SecondaryLogoPath;
	private String SubDomainName;
	private String SuggestionEmailAddress;
	private String UseNewVersion;
	private String UserNameText;
	private String WBSeason;
	private String WellnessProgramName;

	public String getCAHeaderMovement() {
		return CAHeaderMovement;
	}

	public String getCAHeaderNutrition() {
		return CAHeaderNutrition;
	}

	public String getClientAccountCode() {
		return ClientAccountCode;
	}

	public String getClientAccountName() {
		return ClientAccountName;
	}

	public String getClientCustomerServiceEmailID() {
		return ClientCustomerServiceEmailID;
	}

	public String getClientSystemEmailID() {
		return ClientSystemEmailID;
	}

	public String getCommentFromEmail() {
		return CommentFromEmail;
	}

	public String getCommentToEmail() {
		return CommentToEmail;
	}

	public String getCommunityAdminUserID() {
		return CommunityAdminUserID;
	}

	public String getCustomerServicePhone() {
		return CustomerServicePhone;
	}

	public String getDefaultExpertName() {
		return DefaultExpertName;
	}

	public String getDefaultFriendUserID() {
		return DefaultFriendUserID;
	}

	public String getDisableOnlineCoaching() {
		return DisableOnlineCoaching;
	}

	public String getDisableOnsiteCoaching() {
		return DisableOnsiteCoaching;
	}

	public String getDisableSiteHeader() {
		return DisableSiteHeader;
	}

	public String getDisableTelephonicCoaching() {
		return DisableTelephonicCoaching;
	}

	public String getExternalLogoutUrl() {
		return ExternalLogoutUrl;
	}

	public String getFullDomainName() {
		return FullDomainName;
	}

	public String getHideOfficeSelection() {
		return HideOfficeSelection;
	}

	public String getHideTopnav() {
		return HideTopnav;
	}

	public String getIsActive() {
		return IsActive;
	}

	public String getIsCountryBasedClient() {
		return IsCountryBasedClient;
	}

	public String getIsDualLoginEnabled() {
		return IsDualLoginEnabled;
	}

	public String getIsFullDomainNameSupported() {
		return IsFullDomainNameSupported;
	}

	public String getIsHVSyncDisable() {
		return IsHVSyncDisable;
	}

	public String getIsHVToDoBlockEnabled() {
		return IsHVToDoBlockEnabled;
	}

	public String getIsLeftNavEnabled() {
		return IsLeftNavEnabled;
	}

	public String getIsMobileEnabled() {
		return IsMobileEnabled;
	}

	public String getIsPostNotificationEnable() {
		return IsPostNotificationEnable;
	}

	public String getIsRememberMeHidden() {
		return IsRememberMeHidden;
	}

	public String getIsScreeningToDoBlockEnabled() {
		return IsScreeningToDoBlockEnabled;
	}

	public String getIsSilverlightDesktopInstallationSupported() {
		return IsSilverlightDesktopInstallationSupported;
	}

	public String getIsSLDesktopInstallationDisabled() {
		return IsSLDesktopInstallationDisabled;
	}

	public String getIsSSOEnabled() {
		return IsSSOEnabled;
	}

	public String getIsWellnessScoreDisabled() {
		return IsWellnessScoreDisabled;
	}

	public String getNGCLobCode() {
		return NGCLobCode;
	}

	public String getNurturCode() {
		return NurturCode;
	}

	public String getPortalWidth() {
		return PortalWidth;
	}

	public String getPrimaryColor() {
		return PrimaryColor;
	}

	public String getRawScreeningSeason() {
		return RawScreeningSeason;
	}

	public String getRemoveCASpecLinks() {
		return RemoveCASpecLinks;
	}

	public String getSchedulerEmailFromAddress() {
		return SchedulerEmailFromAddress;
	}

	public String getScreeningReminderEmailsEnabled() {
		return ScreeningReminderEmailsEnabled;
	}

	public String getScreeningSeason() {
		return ScreeningSeason;
	}

	public String getSeason() {
		return Season;
	}

	public String getSecondaryColor() {
		return SecondaryColor;
	}

	public String getSecondaryLogoPath() {
		return SecondaryLogoPath;
	}

	public String getSubDomainName() {
		return SubDomainName;
	}

	public String getSuggestionEmailAddress() {
		return SuggestionEmailAddress;
	}

	public String getUseNewVersion() {
		return UseNewVersion;
	}

	public String getUserNameText() {
		return UserNameText;
	}

	public String getWBSeason() {
		return WBSeason;
	}

	public String getWellnessProgramName() {
		return WellnessProgramName;
	}

	public void setCAHeaderMovement(String cAHeaderMovement) {
		CAHeaderMovement = cAHeaderMovement;
	}

	public void setCAHeaderNutrition(String cAHeaderNutrition) {
		CAHeaderNutrition = cAHeaderNutrition;
	}

	public void setClientAccountCode(String clientAccountCode) {
		ClientAccountCode = clientAccountCode;
	}

	public void setClientAccountName(String clientAccountName) {
		ClientAccountName = clientAccountName;
	}

	public void setClientCustomerServiceEmailID(
			String clientCustomerServiceEmailID) {
		ClientCustomerServiceEmailID = clientCustomerServiceEmailID;
	}

	public void setClientSystemEmailID(String clientSystemEmailID) {
		ClientSystemEmailID = clientSystemEmailID;
	}

	public void setCommentFromEmail(String commentFromEmail) {
		CommentFromEmail = commentFromEmail;
	}

	public void setCommentToEmail(String commentToEmail) {
		CommentToEmail = commentToEmail;
	}

	public void setCommunityAdminUserID(String communityAdminUserID) {
		CommunityAdminUserID = communityAdminUserID;
	}

	public void setCustomerServicePhone(String customerServicePhone) {
		CustomerServicePhone = customerServicePhone;
	}

	public void setDefaultExpertName(String defaultExpertName) {
		DefaultExpertName = defaultExpertName;
	}

	public void setDefaultFriendUserID(String defaultFriendUserID) {
		DefaultFriendUserID = defaultFriendUserID;
	}

	public void setDisableOnlineCoaching(String disableOnlineCoaching) {
		DisableOnlineCoaching = disableOnlineCoaching;
	}

	public void setDisableOnsiteCoaching(String disableOnsiteCoaching) {
		DisableOnsiteCoaching = disableOnsiteCoaching;
	}

	public void setDisableSiteHeader(String disableSiteHeader) {
		DisableSiteHeader = disableSiteHeader;
	}

	public void setDisableTelephonicCoaching(String disableTelephonicCoaching) {
		DisableTelephonicCoaching = disableTelephonicCoaching;
	}

	public void setExternalLogoutUrl(String externalLogoutUrl) {
		ExternalLogoutUrl = externalLogoutUrl;
	}

	public void setFullDomainName(String fullDomainName) {
		FullDomainName = fullDomainName;
	}

	public void setHideOfficeSelection(String hideOfficeSelection) {
		HideOfficeSelection = hideOfficeSelection;
	}

	public void setHideTopnav(String hideTopnav) {
		HideTopnav = hideTopnav;
	}

	public void setIsActive(String isActive) {
		IsActive = isActive;
	}

	public void setIsCountryBasedClient(String isCountryBasedClient) {
		IsCountryBasedClient = isCountryBasedClient;
	}

	public void setIsDualLoginEnabled(String isDualLoginEnabled) {
		IsDualLoginEnabled = isDualLoginEnabled;
	}

	public void setIsFullDomainNameSupported(String isFullDomainNameSupported) {
		IsFullDomainNameSupported = isFullDomainNameSupported;
	}

	public void setIsHVSyncDisable(String isHVSyncDisable) {
		IsHVSyncDisable = isHVSyncDisable;
	}

	public void setIsHVToDoBlockEnabled(String isHVToDoBlockEnabled) {
		IsHVToDoBlockEnabled = isHVToDoBlockEnabled;
	}

	public void setIsLeftNavEnabled(String isLeftNavEnabled) {
		IsLeftNavEnabled = isLeftNavEnabled;
	}

	public void setIsMobileEnabled(String isMobileEnabled) {
		IsMobileEnabled = isMobileEnabled;
	}

	public void setIsPostNotificationEnable(String isPostNotificationEnable) {
		IsPostNotificationEnable = isPostNotificationEnable;
	}

	public void setIsRememberMeHidden(String isRememberMeHidden) {
		IsRememberMeHidden = isRememberMeHidden;
	}

	public void setIsScreeningToDoBlockEnabled(
			String isScreeningToDoBlockEnabled) {
		IsScreeningToDoBlockEnabled = isScreeningToDoBlockEnabled;
	}

	public void setIsSilverlightDesktopInstallationSupported(
			String isSilverlightDesktopInstallationSupported) {
		IsSilverlightDesktopInstallationSupported = isSilverlightDesktopInstallationSupported;
	}

	public void setIsSLDesktopInstallationDisabled(
			String isSLDesktopInstallationDisabled) {
		IsSLDesktopInstallationDisabled = isSLDesktopInstallationDisabled;
	}

	public void setIsSSOEnabled(String isSSOEnabled) {
		IsSSOEnabled = isSSOEnabled;
	}

	public void setIsWellnessScoreDisabled(String isWellnessScoreDisabled) {
		IsWellnessScoreDisabled = isWellnessScoreDisabled;
	}

	public void setNGCLobCode(String nGCLobCode) {
		NGCLobCode = nGCLobCode;
	}

	public void setNurturCode(String nurturCode) {
		NurturCode = nurturCode;
	}

	public void setPortalWidth(String portalWidth) {
		PortalWidth = portalWidth;
	}

	public void setPrimaryColor(String primaryColor) {
		PrimaryColor = primaryColor;
	}

	public void setRawScreeningSeason(String rawScreeningSeason) {
		RawScreeningSeason = rawScreeningSeason;
	}

	public void setRemoveCASpecLinks(String removeCASpecLinks) {
		RemoveCASpecLinks = removeCASpecLinks;
	}

	public void setSchedulerEmailFromAddress(String schedulerEmailFromAddress) {
		SchedulerEmailFromAddress = schedulerEmailFromAddress;
	}

	public void setScreeningReminderEmailsEnabled(
			String screeningReminderEmailsEnabled) {
		ScreeningReminderEmailsEnabled = screeningReminderEmailsEnabled;
	}

	public void setScreeningSeason(String screeningSeason) {
		ScreeningSeason = screeningSeason;
	}

	public void setSeason(String season) {
		Season = season;
	}

	public void setSecondaryColor(String secondaryColor) {
		SecondaryColor = secondaryColor;
	}

	public void setSecondaryLogoPath(String secondaryLogoPath) {
		SecondaryLogoPath = secondaryLogoPath;
	}

	public void setSubDomainName(String subDomainName) {
		SubDomainName = subDomainName;
	}

	public void setSuggestionEmailAddress(String suggestionEmailAddress) {
		SuggestionEmailAddress = suggestionEmailAddress;
	}

	public void setUseNewVersion(String useNewVersion) {
		UseNewVersion = useNewVersion;
	}

	public void setUserNameText(String userNameText) {
		UserNameText = userNameText;
	}

	public void setWBSeason(String wBSeason) {
		WBSeason = wBSeason;
	}

	public void setWellnessProgramName(String wellnessProgramName) {
		WellnessProgramName = wellnessProgramName;
	}

	@Override
	public String toString() {
		return "LH_Client [CAHeaderMovement=" + CAHeaderMovement
				+ ", CAHeaderNutrition=" + CAHeaderNutrition
				+ ", ClientAccountCode=" + ClientAccountCode
				+ ", ClientAccountName=" + ClientAccountName
				+ ", ClientCustomerServiceEmailID="
				+ ClientCustomerServiceEmailID + ", ClientSystemEmailID="
				+ ClientSystemEmailID + ", CommentFromEmail="
				+ CommentFromEmail + ", CommentToEmail=" + CommentToEmail
				+ ", CommunityAdminUserID=" + CommunityAdminUserID
				+ ", CustomerServicePhone=" + CustomerServicePhone
				+ ", DefaultExpertName=" + DefaultExpertName
				+ ", DefaultFriendUserID=" + DefaultFriendUserID
				+ ", DisableOnlineCoaching=" + DisableOnlineCoaching
				+ ", DisableOnsiteCoaching=" + DisableOnsiteCoaching
				+ ", DisableSiteHeader=" + DisableSiteHeader
				+ ", DisableTelephonicCoaching=" + DisableTelephonicCoaching
				+ ", WellnessProgramName=" + WellnessProgramName
				+ ", IsScreeningToDoBlockEnabled="
				+ IsScreeningToDoBlockEnabled + "]";
	}

	public String getMobileLogo() {
		return MobileLogo;
	}

	public void setMobileLogo(String mobileLogo) {
		MobileLogo = mobileLogo;
	}

}
