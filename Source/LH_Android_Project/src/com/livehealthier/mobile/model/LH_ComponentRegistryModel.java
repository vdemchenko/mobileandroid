package com.livehealthier.mobile.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LH_ComponentRegistryModel {
	@JsonProperty("Status")
	private String Status;
	@JsonProperty("ComponentRegistryItems")
	private List<LH_ComponentRegistryItems> ComponentRegistryItems;


public void setStatus(String Status) {
	this.Status = Status;
}

public String getStatus() {
	return Status;
}

public List<LH_ComponentRegistryItems> getComponentRegistryItems() {
	return ComponentRegistryItems;
}

public void setComponentRegistryItems(
		List<LH_ComponentRegistryItems> componentRegistryItems) {
	ComponentRegistryItems = componentRegistryItems;
}




}