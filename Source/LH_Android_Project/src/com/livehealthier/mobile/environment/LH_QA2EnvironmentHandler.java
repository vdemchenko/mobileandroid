package com.livehealthier.mobile.environment;

public class LH_QA2EnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "http://qa2.api.v2.qa-web01.livehealthier.com/";
		super.gadgetDeeplinkBaseUrl="http://gadgetv2.qa-web01.livehealthier.com/default.aspx";
		super.analyticsPropertyID = "UA-58583957-2";
		super.isVerboseLoggingEnabled = true;
		super.isDebugLoggingEnabled = true;
	}

}
