package com.livehealthier.mobile.environment;

public class LH_QA3EnvironmentHandler extends LH_EnvironmentHandler {

	@Override
	public void initialize() {
		super.apiBaseUrl = "http://lhapi.qa3.livehealthier.com/";
		super.analyticsPropertyID = "UA-59090009-1";
		
		//super.gadgetDeeplinkBaseUrl="http://gadgetv2.qa-web01.livehealthier.com/default.aspx";
		super.isVerboseLoggingEnabled = true;
		super.isDebugLoggingEnabled = true;
	}

}
