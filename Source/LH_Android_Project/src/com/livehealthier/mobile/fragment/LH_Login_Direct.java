package com.livehealthier.mobile.fragment;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.http.LHConnectionDetector;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_HttpTask;

@SuppressLint("NewApi")
public class LH_Login_Direct extends FragmentActivity {
	public static TextView logindirect;
	EditText user_edit_text, password_edit_text;
	JSONObject loginjson;
	ImageView backbutton;
	ImageView arrownav;
	TextView savepasswordinfo;
	public static WebView loadingimg;
	public static LH_Login_Direct lhloginactivity;
	String deeplinkTitle ="";
	String deeplinkUrl ="";
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// getActionBar().setTitle("Login");
		getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		getActionBar().hide();

		setContentView(R.layout.loginscreen_direct);
		lhloginactivity = this;
		logindirect = (TextView) findViewById(R.id.Loginbutton);
		try {
			Intent intent = getIntent();
			deeplinkTitle = intent.getStringExtra(LH_Constants._TITLE);
			deeplinkUrl = intent.getStringExtra(LH_Constants._URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		backbutton = (ImageView) findViewById(R.id.back_btn);
		user_edit_text = (EditText) findViewById(R.id.user_edit_text);
		password_edit_text = (EditText) findViewById(R.id.password_edit_text);
		arrownav = (ImageView) findViewById(R.id.arrownavigation);
		loadingimg = (WebView) findViewById(R.id.loadingimg);
		// loadingimg.loadUrl("file:///android_asset/loader.gif");
		loadingimg.loadUrl("file:///android_asset/loader_generic.gif");

		loadingimg.setVisibility(View.INVISIBLE);
		password_edit_text.setTypeface(Typeface.DEFAULT);
		password_edit_text
				.setTransformationMethod(new PasswordTransformationMethod());
		savepasswordinfo = (TextView) findViewById(R.id.savepasswordinfo);
		// String text =
		// "<html><body style=\"text-align:justify\">%s</body></Html>";
		// savepasswordinfo.setBackgroundColor(0x00000000);
		// savepasswordinfo.loadData(String.format(text,
		// getString(R.string.savepasswordinstruction_direct)),
		// "text/html", "utf-8");
		final LHConnectionDetector LH = new LHConnectionDetector(
				lhloginactivity);

		arrownav.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				String backgroundImageName = (String) arrownav.getTag();

				if (backgroundImageName.equalsIgnoreCase("downarrow")) {

					savepasswordinfo.setVisibility(View.VISIBLE);
					arrownav.setTag("uparrow");
					arrownav.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_up));

				} else {
					savepasswordinfo.setVisibility(View.INVISIBLE);
					arrownav.setTag("downarrow");
					arrownav.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_down));
				}

			}
		});

		backbutton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		logindirect.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {

				try {

					if (LH.isConnectingToInternet()) {

						if (user_edit_text.getText().toString().trim().length() == 0) {
							Toast.makeText(
									lhloginactivity,
									lhloginactivity.getResources().getString(
											R.string.enterusername),
									Toast.LENGTH_LONG).show();
							user_edit_text.requestFocus();

						} else if (password_edit_text.getText().toString()
								.trim().length() == 0) {
							Toast.makeText(
									lhloginactivity,
									lhloginactivity.getResources().getString(
											R.string.enterpassword),
									Toast.LENGTH_LONG).show();

							password_edit_text.requestFocus();
						} else {

							List<NameValuePair> pairs = new ArrayList<NameValuePair>();
							pairs.add(new BasicNameValuePair("username",
									user_edit_text.getText().toString().trim()));

							pairs.add(new BasicNameValuePair("password",
									password_edit_text.getText().toString()
											.trim()));
							pairs.add(new BasicNameValuePair("deviceName",
									android.os.Build.MODEL));

							logindirect.setPressed(true);
							logindirect.setClickable(false);
							logindirect.setFocusable(true);

							if(null!=deeplinkTitle && deeplinkTitle.length()>2){
								new LH_HttpTask(LHApplication.CurrentEnvironment
										.getApiBaseUrl()
										+ LH_ApiUrls.AUTHENICATEAPI, pairs,
										lhloginactivity, "Login",deeplinkTitle,deeplinkUrl).execute("Login");
								
							}else{
							
							new LH_HttpTask(LHApplication.CurrentEnvironment
									.getApiBaseUrl()
									+ LH_ApiUrls.AUTHENICATEAPI, pairs,
									lhloginactivity, "Login").execute("Login");
							
							
							}
							
							LHApplication.analyticsHandler
									.eventAnalyticsHits(
											getApplicationContext(),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Login_Screen),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Category),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Action_Button_Pressed),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Level_Direct_Login));

						}
					} else {

						final AlertDialog alertDialog = new AlertDialog.Builder(
								lhloginactivity).create();
						alertDialog.setTitle(lhloginactivity.getResources()
								.getString(R.string.Network_heading));
						alertDialog.setMessage(lhloginactivity.getResources()
								.getString(R.string.Network_msg));
						alertDialog.setCancelable(false);
						alertDialog.setButton(lhloginactivity.getResources()
								.getString(R.string.Network_setting),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										try {
											startActivity(new Intent(
													Settings.ACTION_SETTINGS));
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}
								});

						alertDialog.setButton2(lhloginactivity.getResources()
								.getString(R.string.Cancel),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										alertDialog.cancel();

									}
								});

						// alertDialog.setIc

						alertDialog.show();

						// Toast.makeText(
						// lhloginactivity,
						// "Oops! You must be connected to the internet in order to log in. Please check your data and Wi-Fi settings and try again.",
						// Toast.LENGTH_LONG).show();
					}

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

	}

	@Override
	public void onResume() {
		password_edit_text.setText("");
		user_edit_text.requestFocus();

		super.onResume();
	}
}
