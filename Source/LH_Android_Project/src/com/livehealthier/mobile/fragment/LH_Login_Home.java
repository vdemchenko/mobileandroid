package com.livehealthier.mobile.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.utils.LH_Constants;

@SuppressLint("NewApi")
public class LH_Login_Home extends FragmentActivity {
	TextView directTextView, ssoTextview;
	public static LH_Login_Home lhloginhomeactivity;
	String Title ="";
	String Url ="";
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
			getActionBar().hide();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setContentView(R.layout.loginscreen);
		lhloginhomeactivity = this;
		directTextView = (TextView) findViewById(R.id.textView4);

		ssoTextview = (TextView) findViewById(R.id.textView5);
		
		try {
			Intent intent = getIntent();
			Title = intent.getStringExtra(LH_Constants._TITLE);
			Url = intent.getStringExtra(LH_Constants._URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		directTextView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// 2.if clicking a link
				Intent intent = new Intent(LH_Login_Home.this,
						com.livehealthier.mobile.fragment.LH_Login_Direct.class);
				if(null!=Title && Title.length()>2){
				intent.putExtra(LH_Constants._TITLE, Title);
				intent.putExtra(LH_Constants._URL, Url);
				}
				startActivity(intent);
				LHApplication.analyticsHandler.eventAnalyticsHits(
						getApplicationContext(),
						getApplicationContext().getResources().getString(
								R.string.GA_Login_Screen),
						getApplicationContext().getResources().getString(
								R.string.GA_Category),
						getApplicationContext().getResources().getString(
								R.string.GA_Action_Button_Pressed),
						getApplicationContext().getResources().getString(
								R.string.GA_Level_Select_Direct_Login));

			}
		});

		ssoTextview.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(LH_Login_Home.this,
						com.livehealthier.mobile.fragment.LH_Login_SSO.class);
				if(null!=Title && Title.length()>2){
					intent.putExtra(LH_Constants._TITLE, Title);
					intent.putExtra(LH_Constants._URL, Url);
					}
				startActivity(intent);
				LHApplication.analyticsHandler.eventAnalyticsHits(
						getApplicationContext(),
						getApplicationContext().getResources().getString(
								R.string.GA_Login_Screen),
						getApplicationContext().getResources().getString(
								R.string.GA_Category),
						getApplicationContext().getResources().getString(
								R.string.GA_Action_Button_Pressed),
						getApplicationContext().getResources().getString(
								R.string.GA_Level_Select_SSO));

			}
		});

	}
}
