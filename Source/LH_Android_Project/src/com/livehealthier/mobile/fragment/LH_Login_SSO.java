package com.livehealthier.mobile.fragment;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.livehealthier.mobile.R;
import com.livehealthier.mobile.activity.LHApplication;
import com.livehealthier.mobile.google_analytics.LH_Google_Analytics;
import com.livehealthier.mobile.http.LHConnectionDetector;
import com.livehealthier.mobile.utils.LH_ApiUrls;
import com.livehealthier.mobile.utils.LH_Constants;
import com.livehealthier.mobile.utils.LH_HttpTask;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LH_Login_SSO extends FragmentActivity {
	public static TextView loginpasscode;
	EditText passcode_edit_text;
	ImageView backbutton;
	public static LH_Login_SSO lhloginssoactivity;
	ImageView arrownav;
	TextView savepasswordinfo;
	String deeplinkTitle ="";
	String deeplinkUrl ="";
	public static WebView loadingimg;

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		try {
			// getActionBar().setTitle("Login");
			getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
			getActionBar().hide();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lhloginssoactivity = this;

		setContentView(R.layout.login_sso_page);

		getActionBar().setTitle("Login");
		try {
			Intent intent = getIntent();
			deeplinkTitle = intent.getStringExtra(LH_Constants._TITLE);
			deeplinkUrl = intent.getStringExtra(LH_Constants._URL);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final LHConnectionDetector LH = new LHConnectionDetector(
				lhloginssoactivity);

		// backbutton = (ImageView) findViewById(R.id.back_btn);
		// backbutton.setOnClickListener(new View.OnClickListener() {
		//
		// @Override
		// public void onClick(View v) {
		// // TODO Auto-generated method stub
		// finish();
		// }
		// });

		arrownav = (ImageView) findViewById(R.id.arrownavigation);

		loadingimg = (WebView) findViewById(R.id.loadingimg);
		// loadingimg.loadUrl("file:///android_asset/loader.gif");
		loadingimg.loadUrl("file:///android_asset/loader_generic.gif");

		loadingimg.setVisibility(View.INVISIBLE);
		savepasswordinfo = (TextView) findViewById(R.id.textView8);

		final LH_Google_Analytics lh_google_analytics = new LH_Google_Analytics();

		// String text =
		// "<html><body style=\"text-align:justify\">%s</body></Html>";
		//
		// savepasswordinfo.setBackgroundColor(0x00000000);
		// savepasswordinfo.loadData(String.format(text,
		// getString(R.string.savepasswordinstruction)), "text/html",
		// "utf-8");
		loginpasscode = (TextView) findViewById(R.id.Loginbutton);
		passcode_edit_text = (EditText) findViewById(R.id.password_edit_text);
		
		
		
		arrownav.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				String backgroundImageName = (String) arrownav.getTag();

				if (backgroundImageName.equalsIgnoreCase("downarrow")) {

					savepasswordinfo.setVisibility(View.VISIBLE);
					arrownav.setTag("uparrow");
					arrownav.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_up));

				} else {
					savepasswordinfo.setVisibility(View.INVISIBLE);
					arrownav.setTag("downarrow");
					arrownav.setImageDrawable(getResources().getDrawable(
							R.drawable.arrow_down));
				}

			}
		});

		loginpasscode.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				// 2.if clicking a link

				try {
					if (LH.isConnectingToInternet()) {

						if (passcode_edit_text.getText().toString().trim()
								.length() == 0) {
							Toast.makeText(
									lhloginssoactivity,
									lhloginssoactivity
											.getResources()
											.getString(R.string.invalidpasscode),
									Toast.LENGTH_LONG).show();
							
							passcode_edit_text.requestFocus();

						} else {

							List<NameValuePair> pairs = new ArrayList<NameValuePair>();

							pairs.add(new BasicNameValuePair("Token",
									passcode_edit_text.getText().toString()
											.trim()));
							// pairs.add(new BasicNameValuePair("SSOToken ",
							// "C7953980"));
							pairs.add(new BasicNameValuePair("deviceName",
									android.os.Build.MODEL));

							// final ProgressDialog ringProgressDialog =
							// ProgressDialog.show(LH_Login_Direct.this,
							// "Please wait ...", "Downloading Image ...",
							// true);
							// ringProgressDialog.setCancelable(false);
//							loginpasscode.setPressed(true);
//							loginpasscode.setFocusable(true);
							loginpasscode.setClickable(false);
							
							if(null!=deeplinkTitle && deeplinkTitle.length()>2){
								new LH_HttpTask(LHApplication.CurrentEnvironment
										.getApiBaseUrl()
										+ LH_ApiUrls.AUTHENICATEAPI, pairs,
										lhloginssoactivity, "SSO",deeplinkTitle,deeplinkUrl).execute("Login");
								
							}else{
								
								new LH_HttpTask(LHApplication.CurrentEnvironment
										.getApiBaseUrl()
										+ LH_ApiUrls.AUTHENICATEAPI, pairs,
										lhloginssoactivity, "SSO").execute("Login");
								
							}
							
							LHApplication.getComponentRegistryUpdate("Login");
						
							LHApplication.analyticsHandler
									.eventAnalyticsHits(
											getApplicationContext(),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Login_Screen),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Category),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Action_Button_Pressed),
											getApplicationContext()
													.getResources()
													.getString(
															R.string.GA_Level_SSO_Login));

						}
					} else {

						final AlertDialog alertDialog = new AlertDialog.Builder(
								lhloginssoactivity).create();
						alertDialog.setTitle(lhloginssoactivity.getResources()
								.getString(R.string.Network_heading));
						alertDialog
								.setMessage(lhloginssoactivity.getResources()
										.getString(R.string.Network_msg));
						alertDialog.setCancelable(false);
						alertDialog.setButton(lhloginssoactivity.getResources()
								.getString(R.string.Network_setting),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										try {
											startActivity(new Intent(
													Settings.ACTION_SETTINGS));
										} catch (Exception e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}

									}
								});

						alertDialog.setButton2(lhloginssoactivity
								.getResources().getString(R.string.Cancel),
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {

										alertDialog.cancel();

									}
								});

						// alertDialog.setIc

						alertDialog.show();
						// Toast.makeText(
						// lhloginssoactivity,
						// "Oops! You must be connected to the internet in order to log in. Please check your data and Wi-Fi settings and try again.",
						// Toast.LENGTH_LONG).show();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// Intent intent = new Intent(
				// LH_Login_SSO.this,
				// com.livehealthier.mobile.activity.LH_Activity.class);
				// startActivity(intent);
				// finish();
			}
		});
	}

	@Override
	public void onResume() {

		
		loginpasscode.setClickable(true);

		super.onResume();
	}
}
